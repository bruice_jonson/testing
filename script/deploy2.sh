#Create a cluster
aws ecs create-cluster --cluster-name AWSDemo-Prod --region us-west-2

# The AWS registry now has our new container, but our cluster isn't aware that a new version
# of the container is available. We need to create an updated task definition. Task definitions
# always have a version number. When we register a task definition using a name that already
# exists, AWS automatically increments the previously used version number for the task
# definition with that same name and uses it here. Note that we also define CPU and memory
# requirements here and give it a JSON file describing our task definition that I've saved
# to my repository in a aws/ directory.
aws ecs register-task-definition --family run-aws-prod --requires-compatibilities FARGATE --cpu 256 --memory 512  --cli-input-json file://script/demo-task-definition2.json  --region us-west-2

# Create our service.
aws ecs create-service --cluster AWSDemo-Prod --service-name start-aws-demo-prod --task-definition run-aws-prod --desired-count 1 --launch-type "FARGATE" --network-configuration "awsvpcConfiguration={subnets=[subnet-0390b2f90d8f00ecb, subnet-04f3bcbcab988e6c1],securityGroups=[sg-0014625b544509774],assignPublicIp=ENABLED}" --region us-west-2