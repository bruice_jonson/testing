#Setting Region
export AWS_DEFAULT_REGION=$AWS_REGION

#Create a cluster
aws ecs create-cluster --cluster-name AWSDemo

# The AWS registry now has our new container, but our cluster isn't aware that a new version
# of the container is available. We need to create an updated task definition. Task definitions
# always have a version number. When we register a task definition using a name that already
# exists, AWS automatically increments the previously used version number for the task
# definition with that same name and uses it here. Note that we also define CPU and memory
# requirements here and give it a JSON file describing our task definition that I've saved
# to my repository in a aws/ directory.
aws ecs register-task-definition --family run-aws --requires-compatibilities FARGATE --cpu 256 --memory 512  --cli-input-json file://script/demo-task-definition.json  --region $AWS_REGION

# Create our service.
aws ecs create-service --cluster AWSDemo --service-name start-aws-demo --task-definition run-aws --desired-count 1 --launch-type "FARGATE" --network-configuration "awsvpcConfiguration={subnets=[subnet-0085f80913278c90e, subnet-016b540f389768459],securityGroups=[sg-004da678127fbc40e],assignPublicIp=ENABLED}"